<?php

/**
 * Renderer class for Selective In-Place Editor (IPE) behavior.
 */
class panels_selective_ipe extends panels_renderer_ipe {

  public function render_region($region_id, $panes) {
    if (_panels_selective_ipe_region_is_ipe_enabled($region_id)){
      return parent::render_region($region_id, $panes);
    }
    return panels_renderer_standard::render_region($region_id, $panes);
  }

  public function render_pane(&$pane) {
    if (_panels_selective_ipe_region_is_ipe_enabled($pane->panel)){
      return parent::render_pane($pane);
    }
    return panels_renderer_standard::render_pane($pane);
  }

}